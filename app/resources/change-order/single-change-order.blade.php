@extends('layouts.app')
@section('content')
  <div id="date" class="fontAlt">
    <span class="month">@php echo date("M");@endphp</span>
    <span>@php echo date("j");@endphp</span>
  </div>
  @while(have_posts()) @php the_post() @endphp
    @php
    $id = get_the_id();
    $status = get_field('change_order_status', $id);
    $changeOrderClasses = $status.' changed-order';
    @endphp
    <article @php post_class($changeOrderClasses) @endphp>
        <header class="flex">
          <h1 class="entry-title change-order-title">{!! get_the_title() !!}</h1>
          <div class="change-order-status-form">
            @if($status == 'complete')
              <i class="material-icons co-complete">assignment_turned_in</i>
            @elseif($status == 'active')
              <i class="material-icons co-active">assignment</i>
            @else
              <i class="material-icons co-new">assignment_late</i>
            @endif
            @php // update the change order form
            acf_form(array(
                'post_id'		=> get_the_id(),
                'post_title'	=> false,
                'post_content'	=> false,
                'form' => true,
                'new_post'		=> array(
                    'post_type'		=> 'change-order',
                    'post_status'	=> 'publish'
                ),
                'fields' => array('field_5c919b252d0ad'),
                'return' => '%post_url%',
                'submit_value'       => 'Update Status',
                'updated_message'    => 'Updated!' // Redirect to new post url
            ));
            @endphp
          </div>
        </header>
        <div class="the-content">
          <div class="order-update-form">
            @php // update the change order form
            acf_form(
              array(
                'post_id'		=> get_the_id(),
                'post_title'	=> true,
                'post_content'	=> false,
                'form' => true,
                'new_post'		=> array(
                    'post_type'		=> 'change-order',
                    'post_status'	=> 'publish'
                ),
                'field_groups' => array('group_5c8ab9cb17d73'),
                'return' => '%post_url%',
                'submit_value'       => 'Update Change Order',
                'updated_message'    => 'Updated!' // Redirect to new post url
              )
            );
            @endphp
          </div>
        </main>

        
        
        <footer>
          {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
        </footer>
        
      </article>
      @php comments_template('/partials/comments.blade.php') @endphp
  @endwhile
@endsection


  