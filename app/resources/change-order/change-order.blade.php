@extends('layouts.app')
@section('content')
  <div id="date" class="fontAlt">
  <span class="month">@php echo date("M");@endphp</span>
  <span>@php echo date("j");@endphp</span>
  </div>
  <h1 class="latest-change fontAlt">Latest Change</h1>
  <div id="changeOrderFilter">
  @php echo do_shortcode('[searchandfilter fields="status,brand,formula,search" empty_search_url="'.get_bloginfo('url').'"]'); @endphp
  </div>
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif
  <div class="change-orders-wrap">
  <div class="change-order-content">
    <div class="change-order-details title-row">
      <span class="view">View</span>
      <span class="change-order-title">Change Name</span>
      <span class="formula-brand">Formula<br/><small>Brand</small></span>
      <span class="size">Size</span>
      <span class="date">Date Created</span>
      <span class="change-order-status-form">Status</span>
    </div>
  </div>
  <div class="order-list-wrap">
  @while (have_posts()) 
  
  @php the_post() @endphp
    @php 
    //set up data
    global $post;
    $id = $post->ID;
    $formID = 'form'.$id;
    $status = get_post_meta( get_the_ID(), 'change_order_status', true );
    //brand name variation
    $brandName = get_post_meta( get_the_ID(), 'change_order_brand_name', true );
    $brandNameTitle = strip_tags($brandName); // Strip all tags
    $brandNameTitle = str_replace('\"', '', $brandNameTitle);
    $brandNameLink = strip_tags($brandName); // Strip all tags
    $brandNameLink = createSlug($brandNameLink);
    //formula name variation
    $formulaName = get_post_meta( get_the_ID(), 'change_order_formula_name', true );
    $formulaNameTitle = strip_tags($formulaName); // Strip all tags
    $formulaNameTitle = str_replace('\"', '', $brandNameTitle);
    $formulaNameLink = strip_tags($formulaName); // Strip all tags
    $formulaNameLink = createSlug($formulaNameLink);
    //other data
    $secondaryName = get_post_meta( get_the_ID(), 'change_order_secondary_name', true );
    $size = get_post_meta( get_the_ID(), 'change_order_size', true );
    $sku = get_post_meta( get_the_ID(), 'change_order_sku', true );
    $skuChangeNote = get_post_meta( get_the_ID(), 'change_order_sku_change_note', true );
    $dateCreated = get_post_meta( get_the_ID(), 'change_order_date_created', true );
    $requestedBy = get_post_meta( get_the_ID(), 'change_order_requested_by', true );
    $fullUPC = get_post_meta( get_the_ID(), 'change_order_full_upc', true );
    $brandId = get_post_meta( get_the_ID(), 'change_order_brand_id', true );
    $formulaId = get_post_meta( get_the_ID(), 'change_order_formula_id', true );
    $changeOrderClasses = $status.' changed-order';
    @endphp
    <article @php post_class($changeOrderClasses) @endphp>
      <div class="change-order-content">
        <div class="change-order-details">
          <a class="view" href="{{ get_permalink() }}" title="View the change order"><i class="material-icons">visibility</i></a>
          <span class="entry-title change-order-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></span>
          <span class="formula-brand">
            <a href="@php home_url();@endphp/formula/@php echo $formulaNameLink; @endphp" title="@php echo $formulaNameTitle; @endphp">@php echo $formulaName; @endphp</a><br/>
            <small><a href="@php home_url();@endphp/brand/@php echo $brandNameLink; @endphp" title="@php echo $brandNameTitle; @endphp">@php echo $brandName; @endphp</a></small></span>
          <span class="size">{{ $size }}</span>
          <span class="date">{{ $dateCreated }}</span>
          <div class="change-order-status-form">
              @if($status == 'complete')
                <i class="material-icons co-complete">assignment_turned_in</i>
              @elseif($status == 'active')
                <i class="material-icons co-active">assignment</i>
              @else
                <i class="material-icons co-new">assignment_late</i>
              @endif
              @php 
              // use this form to update the statu of a post in the change order blog roll
              acf_form(array(
                  'id'        => $formID,
                  'post_id'		=> $id,
                  'post_title'	=> false,
                  'post_content'	=> false,
                  'form' => true,
                  'new_post'		=> array(
                      'post_type'		=> 'change-order',
                      'post_status'	=> 'publish'
                  ),
                  'fields' => array('field_5c919b252d0ad'),
                  'return' => home_url(),
                  'submit_value'       => 'Update Status',
                  'updated_message'    => 'Updated!'
              ));
              @endphp
          </div>
        </div>
      </div>
    </article>
  @endwhile
  </div>
</div>
  {!! get_the_posts_navigation() !!}
@endsection
