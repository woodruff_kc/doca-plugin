<?php

use PostTypes\PostType;

// set up ACF fields
if( function_exists('acf_add_local_field_group') ):
	acf_add_local_field_group(array(
		'key' => 'group_5c8ab9cb17d73',
		'title' => 'Change Orders',
		'fields' => array(
			array(
				'key' => 'field_5c923f8c8c195',
				'label' => 'Change Order',
				'name' => 'change_order',
				'type' => 'group',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'layout' => 'row',
				'sub_fields' => array(
					array(
						'key' => 'field_5c9111b8c850e',
						'label' => 'Brand Name',
						'name' => 'brand_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c9111c4c850f',
						'label' => 'Formula Name',
						'name' => 'formula_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c9111dcc8510',
						'label' => 'Secondary Name',
						'name' => 'secondary_name',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c9111f7c8512',
						'label' => 'Size',
						'name' => 'size',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '25',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c9111f1c8511',
						'label' => 'SKU',
						'name' => 'sku',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c91124ac8518',
						'label' => 'SKU Change Note',
						'name' => 'sku_change_note',
						'type' => 'textarea',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '100',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => 3,
						'new_lines' => '',
					),
					array(
						'key' => 'field_5c91551df3d98',
						'label' => 'Date Created',
						'name' => 'date_created',
						'type' => 'date_picker',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'display_format' => 'm/d/Y',
						'return_format' => 'm/d/Y',
						'first_day' => 1,
					),
					array(
						'key' => 'field_5c91552ff3d99',
						'label' => 'Requested by',
						'name' => 'requested_by',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '50',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c911204c8513',
						'label' => 'Full UPC',
						'name' => 'full_upc',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c91120fc8514',
						'label' => 'Brand ID',
						'name' => 'brand_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
					array(
						'key' => 'field_5c911231c8516',
						'label' => 'Formula ID',
						'name' => 'formula_id',
						'type' => 'text',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '20',
							'class' => '',
							'id' => '',
						),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'maxlength' => '',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'change-order',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	acf_add_local_field_group(array(
		'key' => 'group_5c919b168f315',
		'title' => 'Change Order Status',
		'fields' => array(
			array(
				'key' => 'field_5c919b252d0ad',
				'label' => 'Status',
				'name' => 'change_order_status',
				'type' => 'select',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'new' => 'New',
					'active' => 'Active',
					'complete' => 'Complete',
				),
				'default_value' => array(
					0 => 'new',
				),
				'allow_null' => 0,
				'multiple' => 0,
				'ui' => 0,
				'return_format' => 'value',
				'ajax' => 0,
				'placeholder' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'change-order',
				),
			),
		),
		'menu_order' => 1,
		'position' => 'side',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'field',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	endif;

// set up change order custom post type 
$labels = array(
	'name'                  => _x('Change Orders', 'Post Type General Name', 'w_change_order'),
	'singular_name'         => _x('Change Order', 'Post Type Singular Name', 'w_change_order'),
	'menu_name'             => __('Change Orders', 'w_change_order'),
	'archives'              => __('Change Order Archives', 'w_change_order'),
	'attributes'            => __('Change Order Attributes', 'w_change_order'),
	'parent_item_colon'     => __('Parent Change Order:', 'w_change_order'),
	'all_items'             => __('All Change Orders', 'w_change_order'),
	'add_new_item'          => __('Add New Change Order', 'w_change_order'),
	'add_new'               => __('Add New', 'w_change_order'),
	'new_item'              => __('New Change Order', 'w_change_order'),
	'edit_item'             => __('Edit Change Order', 'w_change_order'),
	'update_item'           => __('Update Change Order', 'w_change_order'),
	'view_item'             => __('View Change Order', 'w_change_order'),
	'view_items'            => __('View Change Orders', 'w_change_order'),
	'search_items'          => __('Search Change Orders', 'w_change_order'),
	'not_found'             => __('Not found', 'w_change_order'),
	'not_found_in_trash'    => __('Not found in Trash', 'w_change_order'),
	'featured_image'        => __('Featured Image', 'w_change_order'),
	'set_featured_image'    => __('Set featured image', 'w_change_order'),
	'remove_featured_image' => __('Remove featured image', 'w_change_order'),
	'use_featured_image'    => __('Use as featured image', 'w_change_order'),
	'insert_into_item'      => __('Insert into Change Order', 'w_change_order'),
	'uploaded_to_this_item' => __('Uploaded to this Change Order', 'w_change_order'),
	'items_list'            => __('Change Orders list', 'w_change_order'),
	'items_list_navigation' => __('Change Orders list navigation', 'w_change_order'),
	'filter_items_list'     => __('Filter Change Orders list', 'w_change_order'),
);
// Post type options
$options = [
	'supports' => ['title', 'comments', 'custom-fields', 'editor', 'revisions'],
	'taxonomies' => ['brand', 'formula', 'status'],
	'labels'                => $labels,
	'hierarchical'          => false,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 2,
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => true,
	'can_export'            => true,
	'has_archive'           => true,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'capability_type'       => 'post',
	'show_in_rest'          => true,
	'rest_base'             => '/change-orders',
	'rest_controller_class' => 'WP_REST_Posts_Controller',
	'capability_type' => 'post',
];
// Register post type
$changeOrder = new PostType('change-order', $options);
// Set post type dashicon from Dashicons: https://developer.wordpress.org/resource/dashicons/#chart-bar
$changeOrder->icon('dashicons-clipboard');
// Register the "Change Order" post type with WordPress
$changeOrder->register();

// Register Custom Taxonomy
function brand_taxonomy()
{

	$labels = array(
		'name'                       => _x('Brands', 'Taxonomy General Name', 'brand_taxonomy'),
		'singular_name'              => _x('Brand', 'Taxonomy Singular Name', 'brand_taxonomy'),
		'menu_name'                  => __('Brands', 'brand_taxonomy'),
		'all_items'                  => __('All Brands', 'brand_taxonomy'),
		'parent_item'                => __('Parent Brand', 'brand_taxonomy'),
		'parent_item_colon'          => __('Parent Brand:', 'brand_taxonomy'),
		'new_item_name'              => __('New Brand Name', 'brand_taxonomy'),
		'add_new_item'               => __('Add New Brand', 'brand_taxonomy'),
		'edit_item'                  => __('Edit Brand', 'brand_taxonomy'),
		'update_item'                => __('Update Brand', 'brand_taxonomy'),
		'view_item'                  => __('View Brand', 'brand_taxonomy'),
		'separate_items_with_commas' => __('Separate brands with commas', 'brand_taxonomy'),
		'add_or_remove_items'        => __('Add or remove brands', 'brand_taxonomy'),
		'choose_from_most_used'      => __('Choose from the most used', 'brand_taxonomy'),
		'popular_items'              => __('Popular Brands', 'brand_taxonomy'),
		'search_items'               => __('Search Brands', 'brand_taxonomy'),
		'not_found'                  => __('Not Found', 'brand_taxonomy'),
		'no_terms'                   => __('No brands', 'brand_taxonomy'),
		'items_list'                 => __('brands list', 'brand_taxonomy'),
		'items_list_navigation'      => __('brands list navigation', 'brand_taxonomy'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rest_base'                  => 'brands',
		'rest_controller_class'      => 'brands',
	);
	register_taxonomy('brand', array('change-order'), $args);
}
add_action('init', 'brand_taxonomy', 0);

// Register Custom Taxonomy
function formula_taxonomy()
{

	$labels = array(
		'name'                       => _x('Formulas', 'Taxonomy General Name', 'formula_taxonomy'),
		'singular_name'              => _x('Formula', 'Taxonomy Singular Name', 'formula_taxonomy'),
		'menu_name'                  => __('Formula', 'formula_taxonomy'),
		'all_items'                  => __('All Formulas', 'formula_taxonomy'),
		'parent_item'                => __('Parent Formula', 'formula_taxonomy'),
		'parent_item_colon'          => __('Parent Formula:', 'formula_taxonomy'),
		'new_item_name'              => __('New Formula Name', 'formula_taxonomy'),
		'add_new_item'               => __('Add New Formula', 'formula_taxonomy'),
		'edit_item'                  => __('Edit Formula', 'formula_taxonomy'),
		'update_item'                => __('Update Formula', 'formula_taxonomy'),
		'view_item'                  => __('View Formula', 'formula_taxonomy'),
		'separate_items_with_commas' => __('Separate formulas with commas', 'formula_taxonomy'),
		'add_or_remove_items'        => __('Add or remove formulas', 'formula_taxonomy'),
		'choose_from_most_used'      => __('Choose from the most used', 'formula_taxonomy'),
		'popular_items'              => __('Popular Formulas', 'formula_taxonomy'),
		'search_items'               => __('Search Formulas', 'formula_taxonomy'),
		'not_found'                  => __('Not Found', 'formula_taxonomy'),
		'no_terms'                   => __('No formulas', 'formula_taxonomy'),
		'items_list'                 => __('formulas list', 'formula_taxonomy'),
		'items_list_navigation'      => __('formulas list navigation', 'formula_taxonomy'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
		'rest_base'                  => 'formula',
		'rest_controller_class'      => 'formulas',
	);
	register_taxonomy('formula', array('change-order'), $args);
}
add_action('init', 'formula_taxonomy', 0);

// Register Custom Taxonomy
function status_taxonomy()
{

	$labels = array(
		'name'                       => _x('Status', 'Taxonomy General Name', 'status_taxonomy'),
		'singular_name'              => _x('Status', 'Taxonomy Singular Name', 'status_taxonomy'),
		'menu_name'                  => __('Status', 'status_taxonomy'),
		'all_items'                  => __('All Status', 'status_taxonomy'),
		'parent_item'                => __('Parent Status', 'status_taxonomy'),
		'parent_item_colon'          => __('Parent Status:', 'status_taxonomy'),
		'new_item_name'              => __('New Status Name', 'status_taxonomy'),
		'add_new_item'               => __('Add New Status', 'status_taxonomy'),
		'edit_item'                  => __('Edit Status', 'status_taxonomy'),
		'update_item'                => __('Update Status', 'status_taxonomy'),
		'view_item'                  => __('View Status', 'status_taxonomy'),
		'separate_items_with_commas' => __('Separate status with commas', 'status_taxonomy'),
		'add_or_remove_items'        => __('Add or remove status', 'status_taxonomy'),
		'choose_from_most_used'      => __('Choose from the most used', 'status_taxonomy'),
		'popular_items'              => __('Popular Status', 'status_taxonomy'),
		'search_items'               => __('Search Status', 'status_taxonomy'),
		'not_found'                  => __('Not Found', 'status_taxonomy'),
		'no_terms'                   => __('No status', 'status_taxonomy'),
		'items_list'                 => __('status list', 'status_taxonomy'),
		'items_list_navigation'      => __('status list navigation', 'status_taxonomy'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'show_in_rest'               => true,
		'rest_base'                  => 'status',
		'rest_controller_class'      => 'status',
	);
	register_taxonomy('status', array('change-order'), $args);
}
add_action('init', 'status_taxonomy', 0);

// Enque scripts and styles
add_action('wp_enqueue_scripts', 'wdfChangeOrderEnqueue');
function wdfChangeOrderEnqueue()
{
	//TODO conditionally wrap this to only show on the correct pages
	//styles 
	wp_enqueue_style('wdf_change_order_styles', plugin_dir_url(__FILE__) . '/css/wdf-change-order-public.css', '', '1.0');
	//scripts
	wp_register_script('wdf_change_order_scripts', plugin_dir_url(__FILE__) . '/js/wdf-change-order-public.js', array('jquery'), '1.1', true);
	wp_enqueue_script('wdf_change_order_scripts');
}

// Point to custom templates
add_filter('template_include', function ($template) {
	if (is_single() && get_post_type() == 'change-order') {
		$blade_template = CHANGEORDER_PLUGIN_DIR . "app/resources/change-order/single-change-order.blade.php";
		return ($blade_template) ? $blade_template : $template;
	} elseif (is_search() || is_category() || is_archive() && get_post_type() == 'change-order') {
		$blade_template = CHANGEORDER_PLUGIN_DIR . "app/resources/change-order/change-order.blade.php";
		return ($blade_template) ? $blade_template : $template;
	}
	return $template;
}, 100);


// Add ACF form head 
add_action('wp_head', 'wdf_acf_form_header');
function wdf_acf_form_header(){
	if(is_front_page() || is_archive() || is_singular('change-order')){
		acf_form_head();
	}
};

// this pulls data into the single change order template
add_filter('sage/template/single-change-order/data', 'wdf_change_order_data');
function wdf_change_order_data(array $data)
{
	global $post;
	if ($post->post_type != 'change-order') { return; }
	$id = $post->ID;
	$status = get_post_meta( get_the_ID(), 'change_order_status', true );
	$data['status'] = $status;
	$data['brandName'] = get_post_meta( get_the_ID(), 'change_order_brand_name', true );
	$data['formualName'] = get_post_meta( get_the_ID(), 'change_order_formula_name', true );
	$data['secondaryName'] = get_post_meta( get_the_ID(), 'change_order_secondary_name', true );
	$data['size'] = get_post_meta( get_the_ID(), 'change_order_size', true );
	$data['sku'] = get_post_meta( get_the_ID(), 'change_order_sku', true );
	$data['skuChangeNote'] = get_post_meta( get_the_ID(), 'change_order_sku_change_note', true );
	$data['dateCreated'] = get_post_meta( get_the_ID(), 'change_order_date_created', true );
	$data['requestedBy'] = get_post_meta( get_the_ID(), 'change_order_requested_by', true );
	$data['fullUPC'] = get_post_meta( get_the_ID(), 'change_order_full_upc', true );
	$data['brandId'] = get_post_meta( get_the_ID(), 'change_order_brand_id', true );
	$data['formulaId'] = get_post_meta( get_the_ID(), 'change_order_formula_id', true );
	return $data;
}

// On save set custom taxonomies based on acf fields.
function wdf_change_order_add_tax($post_id)
{
	global $post;
	$id = get_the_ID();
	if ($post->post_type != 'change-order') { return; }
	$post_brands[] = get_post_meta( $id, 'change_order_brand_name', true );
	$post_formulas[] = get_post_meta( $id, 'change_order_formula_name', true );
	$post_status[] = get_post_meta( $id, 'change_order_status', true );
	// set terms
	wp_set_post_terms($id, $post_brands, 'brand', false);
	wp_set_post_terms($id, $post_formulas, 'formula', false);
	wp_set_post_terms($id, $post_status, 'status', false);
}
add_action('save_post', 'wdf_change_order_add_brand_tax', 20);
add_action('draft_to_publish', 'wdf_change_order_add_brand_tax', 20);
add_action('acf/save_post', 'wdf_change_order_add_tax', 20 );

// On form submit in the blog roll set custom taxonomies based on acf fields for specific post.
function wdf_acf_submit_form_loop( $form, $post_id ) {
	$id = $form['post_id'];
	$post_brands[] = get_post_meta( $id, 'change_order_brand_name', true );
	$post_formulas[] = get_post_meta( $id, 'change_order_formula_name', true );
	$post_status[] = get_post_meta( $id, 'change_order_status', true );
	// set terms
	wp_set_post_terms($id, $post_brands, 'brand', false);
	wp_set_post_terms($id, $post_formulas, 'formula', false);
	wp_set_post_terms($id, $post_status, 'status', false);
}
add_action('acf/submit_form', 'wdf_acf_submit_form_loop', 11, 2);
add_action( 'gform_after_submission', 'wdf_acf_submit_form_loop', 10, 2 );

// make change order the main blog items
add_action("pre_get_posts", "wdf_custom_front_page");
function wdf_custom_front_page($wp_query)
{
	//Ensure this filter isn't applied to the admin area
	if (is_admin()) { return; }
	if ($wp_query->get('page_id') == get_option('page_on_front')) :
		$wp_query->set('post_type', 'change-order');
		$wp_query->set('page_id', ''); //Empty
		//Set properties that describe the page to reflect that
		//we aren't really displaying a static page
		$wp_query->is_page = 0;
		$wp_query->is_singular = 0;
		$wp_query->is_post_type_archive = 1;
		$wp_query->is_archive = 1;
	endif;
}

// Move chain select CSV to uploads folder
add_filter('gform_chainedselects_import_file', 'wdf_set_import_file', 10, 3);
function wdf_set_import_file($import_file, $form, $field)
{
	$uploads = wp_upload_dir();
	$csvLocation = $uploads['baseurl'] . '/doca-master.csv';
	$file_array = array(
		'url' => $csvLocation,
		'expiration' => 60
	);
	return $file_array;
}

// turn a string into something that would work as a slug
function createSlug($str, $delimiter = '-'){
	$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
	return $slug;
}

/**
 * Halt the main query in the case of an empty search 
 */
add_filter( 'posts_search', function( $search, \WP_Query $q )
{
    if( ! is_admin() && empty( $search ) && $q->is_search() && $q->is_main_query() )
        $search .=" AND 0=1 ";

    return $search;
}, 10, 2 );