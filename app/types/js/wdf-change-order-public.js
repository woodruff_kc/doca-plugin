(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	
	 
	$('select#acf-field_5c919b252d0ad').on('change', function() {
		var $form = $(this).closest('form');
		$form.find('input[type=submit]').click();
	});
	

	$('.gfield_chainedselect select:eq(3)').on('change', function() {
		var nextSelect = $(this).parent().next().find('select');
		setTimeout(function(){
			var optionValue = $(nextSelect).find( 'option:last-of-type').val();
			$(nextSelect).find( '> option.gf_placeholder').removeAttr('selected');
			$(nextSelect).find( '> option:not(.gf_placeholder)').attr('selected','selected');		
			$(nextSelect).val(optionValue);
			$(nextSelect).change();
			var nextActivate = $(nextSelect).parent().next().find('select');
			$(nextActivate).removeAttr('disabled');
		}, 1000);
	});
	$('.gfield_chainedselect select:eq(4)').on('change', function() {
		var nextSelect = $(this).parent().next().find('select');
		setTimeout(function(){
			var optionValue = $(nextSelect).find( 'option:last-of-type').val();
			$(nextSelect).find( '> option.gf_placeholder').removeAttr('selected');
			$(nextSelect).find( '> option:not(.gf_placeholder)').attr('selected','selected');		
			$(nextSelect).val(optionValue);
			$(nextSelect).change();
			var nextActivate = $(nextSelect).parent().next().find('select');
			$(nextActivate).removeAttr('disabled');
		}, 1000);
	});
	$('.gfield_chainedselect select:eq(5)').on('change', function() {
		var nextSelect = $(this).parent().next().find('select');
		setTimeout(function(){
			var optionValue = $(nextSelect).find( 'option:last-of-type').val();
			$(nextSelect).find( '> option.gf_placeholder').removeAttr('selected');
			$(nextSelect).find( '> option:not(.gf_placeholder)').attr('selected','selected');		
			$(nextSelect).val(optionValue);
			$(nextSelect).change();
			var nextActivate = $(nextSelect).parent().next().find('select');
			$(nextActivate).removeAttr('disabled');
		}, 1000);
	});
	$('.gfield_chainedselect select:eq(6)').on('change', function() {
		var nextSelect = $(this).parent().next().find('select');
		setTimeout(function(){
			var optionValue = $(nextSelect).find( 'option:last-of-type').val();
			$(nextSelect).find( '> option.gf_placeholder').removeAttr('selected');
			$(nextSelect).find( '> option:not(.gf_placeholder)').attr('selected','selected');		
			$(nextSelect).val(optionValue);
			$(nextSelect).change();
		}, 1000);
	});
	


})( jQuery );
