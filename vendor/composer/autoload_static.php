<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita62d7c789928749b52ee47fd3d6a3c5t
{
    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'WDF\\CHANGEORDER\\' => 16,
        ),
        'P' => 
        array (
            'PostTypes\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'WDF\\CHANGEORDER\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
        'PostTypes\\' => 
        array (
            0 => __DIR__ . '/..' . '/jjgrainger/posttypes/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita62d7c789928749b52ee47fd3d6a3c5t::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita62d7c789928749b52ee47fd3d6a3c5t::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
