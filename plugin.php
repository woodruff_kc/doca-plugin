<?php
/*
Plugin Name: DOCA Change Orders
Plugin URI:
Description: DOCA Change Order Custom Post Type (no setting necessary).
Version: 1.0
Author: Joel Schlotterer
Author URI:
License: MIT
*/

namespace WDF\CHANGEORDER;


//Set up autoloader
require __DIR__ . '/vendor/autoload.php';

//Define Constants
define( 'CHANGEORDER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'CHANGEORDER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Autoload the Init class
$example_init = new Init();
